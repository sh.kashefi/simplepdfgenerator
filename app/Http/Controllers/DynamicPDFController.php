<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\PDF;

class DynamicPDFController extends Controller
{
    public function index()
    {
        global $contents;
        $customer_data = $this->get_customer_data();
        $view = view('dynamic_pdf')->with('customer_data', $customer_data);
        $contents = $view->render();
//
        return $view;
    }

    function get_customer_data()
    {
        $customer_data = DB::table('tbl_customer')
         ->limit(10)->get();
        return $customer_data;
    }

    function pdf()
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->index());
        return $pdf->stream();
    }

//    public function convert_customer_data_to_html()
//    {
//        $customer_data = $this->contents;
//        $customer_data = $this->get_customer_data();
//        $output = '
//<h3 align="center">Customer Data</h3>
//     <table width="100%" style="border-collapse: collapse; border: 0px;">
//      <tr>
//    <th style="border: 1px solid; padding:12px;" width="20%">Name</th>
//    <th style="border: 1px solid; padding:12px;" width="30%">Address</th>
//    <th style="border: 1px solid; padding:12px;" width="15%">City</th>
//    <th style="border: 1px solid; padding:12px;" width="15%">Postal Code</th>
//    <th style="border: 1px solid; padding:12px;" width="20%">Country</th>
//   </tr>
//     ';
//        foreach($customer_data as $customer)
//        {
//            $output .= '
//      <tr>
//       <td style="border: 1px solid; padding:12px;">'.$customer->CustomerName.'</td>
//       <td style="border: 1px solid; padding:12px;">'.$customer->Address.'</td>
//       <td style="border: 1px solid; padding:12px;">'.$customer->City.'</td>
//       <td style="border: 1px solid; padding:12px;">'.$customer->PostalCode.'</td>
//       <td style="border: 1px solid; padding:12px;">'.$customer->Country.'</td>
//      </tr>
//      ';
//        }
//        $output .= '</table>';
//        return $output;
//    }
}
